﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        // on left mouse click
        if ( Input.GetButtonDown( "Fire1" ) )
        {
            //get the position of the mouse on the screen
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            //gets the direction of the point clicked from the center of the camera
            Vector3 FireDirection = clickPoint - this.transform.position;
            // normalizes the direction (vector length = 1)
            FireDirection.Normalize();
            //creates a prefab  and fires onto the screen in the direction of the click, from the center of the screen
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }

}
