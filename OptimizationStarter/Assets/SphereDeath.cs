﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereDeath : MonoBehaviour {

    public float timeAlive;
    public float lifeLength;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        timeAlive += Time.deltaTime;

        if (timeAlive > lifeLength)
        {
            KillSelf();
        }

	}



    public void KillSelf()
    {
        Destroy(this.gameObject);

    }
}
