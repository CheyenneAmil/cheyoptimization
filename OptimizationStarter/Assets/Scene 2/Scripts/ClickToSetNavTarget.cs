﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    public new Camera camera;
    public NavMeshAgent agent;
    public Vector3 destination;
    void Awake()
    {
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        agent=GetComponent<NavMeshAgent>();
        transform.position = destination;
    }

    // Update is called once per frame
    void Update ()
    {
        //only ray cast on button press
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                destination = hit.point;
            }
        } 

        // if the game object isnt close to the click destination then move to it.
        if (Vector3.Distance(transform.position, destination) > 1)
        {
            agent.destination = destination;
        }     

       

	}

}
