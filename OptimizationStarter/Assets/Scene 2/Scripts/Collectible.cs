﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
        // creates an array of colliders  for every object overlapping a within a distance of a radius
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );

        // for every collider in the array colliding colliders
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            // if its the player destroy the object
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                Destroy( this.gameObject );
            }
        }
    }

}
