﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        // if there is no collectable object
        if ( m_currentCollectible == null )
        {
            //spawn it in one the location of of a spawn point which are children of this game object.
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.GetChildCount() ) ).position, Collectible.transform.rotation );
        }
    }
}
